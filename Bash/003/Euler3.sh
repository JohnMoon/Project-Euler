#!/bin/sh

function is_prime() {
	x=$1

	if [ $(($x % 2)) -eq 0 ]; then
		sqrt=$((sqrt(x) + 1))
		for i in {3..sqrt}
		do
			if [ $(($x % $i)) -eq 0 ]; then
				return false
			fi
		done
		return true
	else
		return false
	fi
}

NUM=600851475143
UP_TO=$((sqrt($NUM) + 1))
LRG_PRM_FCTR=0

for i in {3..$UP_TO}
do
	if [ $(($NUM % $i)) -eq 0 ] && is_prime $i; then
		LRG_PRM_FCTR=$i
	fi
done

echo "Largest prime factor of $NUM: $LRG_PRM_FCTR"
