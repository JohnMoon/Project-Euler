#!/bin/bash

SUM=0
X=1
Y=1
FIB=0

while [ $FIB -lt 4000000 ]
do
	let "FIB = X + Y"
	let "X = Y"
	let "Y = FIB"
	if [ $(($FIB % 2)) -eq 0 ]; then
		let "SUM += FIB"
	fi
done

echo "Sum: $SUM"
