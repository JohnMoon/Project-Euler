#!/bin/bash

SUM=0

for i in {1..999}
do
	if [ $(($i % 3)) -eq 0 ] || [ $(($i % 5)) -eq 0 ]; then
	   let "SUM += i"
	fi
done

echo "Sum: $SUM"
