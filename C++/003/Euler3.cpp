#include <iostream>
#include <cmath>

using namespace std;

#define NUM 600851475143

bool is_prime(long x);

int main() {
	long up_to = sqrt(NUM);
	int largest_prime_factor = 1;
	for (long i = 3; i < up_to; i = i + 2) {
		if (NUM % i == 0 && is_prime(i))
			largest_prime_factor = i;
	}

	cout << "Largest prime factor of " << NUM << ": " << largest_prime_factor << endl;

	return 0;
}

bool is_prime(long x) {
	if ((x & 1) == 1) {
		long i;
		for (i = 3; i < sqrt(x); i = i + 2) {
			if (x % i == 0)
				return false;
		}
		return true;
	}
	return false;
}
