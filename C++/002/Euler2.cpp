#include <iostream>

using namespace std;

int main() {
	int sum = 0;
	int x = 1;
	int y = 1;
	int fib = 0;
	while (fib < 4000000) {
		fib = x + y;
		x = y;
		y = fib;
		if (fib % 2 == 0)
			sum += fib;
	}

	cout << "Sum: " << sum << endl;

	return 0;
}
