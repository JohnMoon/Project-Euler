These are my solutions to [Project Euler](http://projecteuler.net) problems.

Once I've solved a problem, I try to implement the solution in different
languages I know. If you're interested, I also use the [perf](https://perf.wiki.kernel.org/index.php/Main_Page)
Linux utility to measure the compare the performance of my algorithms in
the different languages. The perf outputs can be found in the "Performance
Comparisons" folder.
