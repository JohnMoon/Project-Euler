const N: u64 = 2_000_000;

fn main() {
    println!("Sum of primes below {}: {}", N, sum_of_primes_below(N));
}

fn sum_of_primes_below(bel: u64) -> u64 {
    let mut n = 3u64;
    let mut sum = 2u64;
    while n < bel {
        if is_prime(n) {
            sum += n;
        }
        n += 2;
    }
    sum
}

fn is_prime(x: u64) -> bool {
    if x == 2 {
        return true;
    }

    if x % 2 != 0 {
        let mut i = 3;
        while i < (x as f64).sqrt() as u64 + 1 {
            if x % i == 0 {
                return false;
            }
            else {
                i += 2;
            }
        }
        true
    } else {
        false
    }
}
