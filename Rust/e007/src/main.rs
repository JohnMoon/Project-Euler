const TARGET_PRIME: u64 = 10_001;

fn main() {
    let mut candidate: u64 = 3;
    let mut num_primes_found = 1;

    if TARGET_PRIME != 1 {
        loop {
            if is_prime(candidate) {
                num_primes_found += 1;
                if num_primes_found == TARGET_PRIME {
                    break;
                }
            }
            candidate += 2;
        }
    } else {
        candidate = 2;
    }
    println!("Prime {} is {}", TARGET_PRIME, candidate);
}

fn is_prime(x: u64) -> bool {
    if x == 2 {
        return true;
    }

    if x % 2 != 0 {
        let mut i = 3;
        while i < (x as f64).sqrt() as u64 + 1 {
            if x % i == 0 {
                return false;
            }
            else {
                i += 2;
            }
        }
        true
    } else {
        false
    }
}
