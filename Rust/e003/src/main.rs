const N_U: u64 = 600_851_475_143;
const N_F: f64 = 600_851_475_143.0;

fn main() {

    let up_to: u64 = N_F.sqrt().round() as u64;
    let mut i = 3;
    let mut largest_prime_factor = 1;
    while i < up_to {
        if N_U % i == 0 && is_prime(i) {
            largest_prime_factor = i;
        }
        i += 2;
    }

    println!("Largest prime factor of {}: {}", N_U, largest_prime_factor);
}

fn is_prime(x: u64) -> bool {
    if x % 2 != 0 {
        let mut i = 3;
        while i < (x as f64).sqrt() as u64 + 1 {
            if x % i == 0 {
                return false;
            } else {
                i += 2;
            }
        }
        true
    } else {
        false
    }
}
