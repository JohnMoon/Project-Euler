const OVER_N_DIVISORS: u64 = 500;

fn get_triangle_num(n: u64) -> u64 {
    (n * (n + 1)) / 2
}

fn get_num_divisors(n: u64) -> u64 {
    if n == 1 {
        1
    } else {
        /* Form two vectors that, when concatenated, contain all factors of n.
         * Start with every n being divisible by 1 and itself. */
        let mut left = vec![1];
        let mut right = vec![n];
        let mut i = 2;

        /* Grow vector until left candidate >= first right-side factor */
        while i <= *right.first().unwrap() {
            if n % i == 0 {
                left.push(i);
                right.insert(0, (n / i) as u64);
            }

            i += 1;
        }
        /* The sum of the two vectors is the number of factors */
        (left.len() + right.len()) as u64
    }
}

fn main() {
    let mut n: u64 = 1;
    loop {
        let tri_num = get_triangle_num(n);
        let n_divs = get_num_divisors(tri_num);
        if n_divs > OVER_N_DIVISORS {
            println!("First triangle num over {} divisors: {}", OVER_N_DIVISORS,
                     tri_num);
            break;
        }

        n += 1;
    }
}
