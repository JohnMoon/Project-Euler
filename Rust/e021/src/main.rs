use std::collections::HashMap;

const UP_TO: u64 = 10_000;

struct Pair {
    a: u64,
    b: u64,
}

impl Pair {
    fn is_amicable(&self, cache: &mut HashMap<u64, u64>) -> bool {
        /* Every known pair is either both even or both odd - if parity differs,
         * the pair is (probably) not amicable. Also A cannot = B */
        if (self.a % 2) != (self.b % 2) ||
            self.a == self.b {
            false
        } else {
            sum_of_factors(self.a, cache) == self.b &&
            sum_of_factors(self.b, cache) == self.a
        }
    }
}

fn sum_of_factors(n: u64, cache: &mut HashMap<u64, u64>) -> u64 {

    /* If we've already calculated this n, no need to calculate it again */
    if let Some(sum) = cache.get(&n) {
        return *sum
    }

    if n == 1 {
        1
    } else {
        /* Form two vectors that, when concatenated, contain all factors of n.
         * Start with every n being divisible by 1 and itself. */
        let mut left = vec![1];
        let mut right = vec![n];
        let mut i = 2;

        /* Grow vector until left candidate >= first right-side factor */
        while i < *right.first().unwrap() {
            if n % i == 0 {
                left.push(i);
                right.insert(0, (n / i) as u64);
            }

            i += 1;
        }

        /* Append right to left */
        left.append(&mut right);

        /* We don't count N itself in the sum */
        left.pop();

        /* Add result to the cache and return it */
        let sum = left.iter().sum();
        cache.insert(n, sum);
        sum
    }
}

fn main() {
    /* Cache to store results of factor sum calculations */
    let mut cache = HashMap::new();

    let p1 = Pair {
        a: 16,
        b: 44,
    };

    assert!(!p1.is_amicable(&mut cache));

    let p2 = Pair {
        a: 220,
        b: 284,
    };
    assert!(p2.is_amicable(&mut cache));

    let mut amicable_pair_sums = Vec::new();
    let mut x = 1;
    while x < UP_TO {
        let mut y = x + 1;
        while y < UP_TO {
            let p = Pair {a: x, b: y};
            if p.is_amicable(&mut cache) {
                println!("({}, {})", x, y);
                amicable_pair_sums.push(x + y);
                x = y;
            } else {
                y += 1;
            }
        }
        x += 1;
    }

    /* Sort and remove duplicates */
    amicable_pair_sums.sort();
    amicable_pair_sums.dedup();

    let sum: u64 = amicable_pair_sums.iter().sum();

    println!("Sum of all amicable numbers under {}: {}", UP_TO, sum);
}
