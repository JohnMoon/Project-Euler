const N: u32 = 100;

fn main() {
    let diff = square_of_sums(N) - sum_of_squares(N);
    println!("Difference: {}", diff);
}

fn square_of_sums(up_to: u32) -> u64 {
    let mut sum: u64 = 0;
    for i in 1..=up_to {
        sum += u64::from(i);
    }
    sum * sum
}

fn sum_of_squares(up_to: u32) -> u64 {
    let mut sum: u64 = 0;
    for i in 1..=up_to {
        sum += u64::from(i * i);
    }
    sum
}
