fn main() {
    let mut largest = 1;
    let mut l_i = 0;
    let mut l_j = 0;

    for i in 1..999 {
        for j in 1..999 {
            let test = i * j;
            if is_palindromic(test) && test > largest {
                largest = test;
                l_i = i;
                l_j = j;
            }
        }
    }
    println!("The largest palindrome from two 3-digit numbers: {} ({} * {})",
                largest, l_i, l_j);
}

fn is_palindromic(num: u32) -> bool {
    let num_s = num.to_string();
    let len = num_s.len();
    let halfway = (len / 2) as usize;
    let half1 = {
        if len % 2 == 0 {
            num_s.get(0..halfway).unwrap()
        } else {
            num_s.get(0..=halfway).unwrap()
        }
    };
    let half2 = num_s.get(halfway..).unwrap();
    let half2_rev = half2.chars().rev().collect::<String>();

    half1 == half2_rev
}
