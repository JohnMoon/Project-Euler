const DAYS_PER_WEEK: u32 = 7;
const MONTHS_PER_YEAR: u8 = 12;

fn days_in_month(month: u8, leap_year: bool) -> u8 {
    match month {
        4 | 6 | 9 | 11 => 30,
        1 | 3 | 5 | 7 | 8 | 10 | 12 => 31,
        2 => {
            if leap_year {
                29
            } else {
                28
            }
        },
        _ => {
            println!("Invalid month {}", month);
            panic!();
        }
    }
}

fn is_leap_year(year: u64) -> bool {
    if year % 4 == 0 {
        if year % 100 != 0 {
            year % 400 != 0
        } else {
            true
        }
    } else {
        false
    }
}

struct Day {
    day: u8,
    month: u8,
    year: u64,
}

impl Day {
    fn inc_day(&mut self) {
        let days_in_month = days_in_month(self.month, is_leap_year(self.year));
        self.day += 1;

        if self.day > days_in_month {
            self.day = 1;
            self.month += 1;
            if self.month > MONTHS_PER_YEAR {
                self.month = 1;
                self.year += 1;
            }
        }
    }

    fn inc_week(&mut self) {
        for _i in 0..DAYS_PER_WEEK {
            self.inc_day();
        }
    }
}

fn main() {
    let mut date = Day {
        day: 6,
        month: 1,
        year: 1901,
    };

    let mut sundays = 0;
    /* Covering the whole century */
    while date.year < 2001 {
        /* Every day that is a multiple of seven days from our start is
         * a Sunday */
        date.inc_week();
        if date.day == 1 {
            sundays += 1;
        }
    }

    println!("Number of 1st of the month Sundays in 20th century: {}", sundays);
}
