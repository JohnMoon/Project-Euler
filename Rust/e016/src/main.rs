const POW: u64 = 1000;

/* Flattens a vector out into integers with just a one's place, carrying digits
 * when necessary */
fn flatten_carry(nums: &[u64]) -> Vec<u64> {
    let mut ret_vec = Vec::new();
    let mut extras = vec![0];
    let mut need_another_recursion = false;
    for num in nums.iter().rev() {
        let ones_place = *num % 10;
        let extra = (((*num - ones_place) as f64) * 0.1) as u64;

        if extra > 0 {
            need_another_recursion = true;
        }

        extras.insert(0, extra);

        ret_vec.push(ones_place + extras.pop().unwrap());
    }

    let extra = extras.pop().unwrap();
    if extra > 0 {
        ret_vec.push(extra);
    }

    /* Undo the reversal we did in the calculation */
    ret_vec.reverse();

    if need_another_recursion {
        flatten_carry(&ret_vec)
    } else {
        ret_vec
    }
}

fn main() {
    let mut num_vec: Vec<u64> = Vec::new();
    num_vec.push(2);

    for _n in 0..(POW - 1) {
        /* Multiply all elements by 2 */
        num_vec = num_vec.into_iter().map(|x| x * 2).collect();

        /* Flatten the vector */
        num_vec = flatten_carry(&num_vec);
    }

    println!("2^{}:", POW);
    let mut sum = 0;
    for n in num_vec {
        print!("{}", n);
        sum += n;
    }
    println!("\n");
    println!("Sum of digits in 2^{}: {}", POW, sum);
}
