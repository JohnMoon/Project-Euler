use std::fs::File;
use std::io::{BufRead, BufReader};
use std::cmp;

/* Run with p067_triangle.txt to solve problem 67, p018, for problem 18 */
//static FILE: &str = "p067_triangle.txt";
static FILE: &str = "p018_triangle.txt";

struct Node {
   pub val: u64,
   pub largest_path_val: u64
}

impl Copy for Node { }

impl Clone for Node {
    fn clone(&self) -> Node { *self }
}

fn populate_path_vals(mut triangle: Vec<Vec<Node>>) {
    /* Start at the bottom */
    let bottom_row = triangle.len() - 1;
    let mut row = bottom_row;
    let mut prev_row = triangle[bottom_row].to_vec();
    loop {
        if row == bottom_row {
            for node in triangle[row].iter_mut() {
                node.largest_path_val = node.val;
            }
        } else {
            /* Choose the max of the two pre-calculated nodes below current
             * node */
            for (i, node) in triangle[row].iter_mut().enumerate() {
                node.largest_path_val = node.val +
                    cmp::max(prev_row[i].largest_path_val,
                             prev_row[i + 1].largest_path_val);
            }
        }

        prev_row = triangle[row].to_vec();

        if row == 0 {
            break;
        } else {
            row -= 1;
        }
    }

    println!("Largest path through triangle in {}: {}",
             FILE, triangle[0][0].largest_path_val);
}

fn main() {
    let file = File::open(FILE)
	.expect("error opening file");
    let reader = BufReader::new(file);

    let mut triangle: Vec<Vec<Node>> = Vec::new();

    for line in reader.lines() {
        let mut new_node_vec = Vec::new();
        for num in line.unwrap().split(' ') {
            new_node_vec.push(Node {
                val: num.parse().unwrap(),
                largest_path_val: 0,
            });
        }
        triangle.push(new_node_vec);
    }

    populate_path_vals(triangle);
}
