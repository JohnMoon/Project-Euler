use std::fs::File;
use std::io::{BufRead, BufReader};

static NAME_FILE: &str = "names.txt";
const ASCII_VAL_A: u64 = 64;

/* Note, this assumes all names are UPPERCASE and using only ASCII chars */
fn get_name_score(name: &str, position: u64) -> u64 {
    let mut score = 0;
    for letter in name.chars() {
        score += (letter as u64) - ASCII_VAL_A
    }
    score * position
}

fn main() {
    let name_f = File::open(NAME_FILE)
        .expect("error opening file");
    let reader = BufReader::new(name_f);

    let mut names = Vec::new();
    for line in reader.lines() {
        for name in line.unwrap().split(',') {
            /* Insert such that we get a sorted vector of names */
            match names.binary_search(&name.to_string()) {
                Ok(_) => (),
                Err(pos) => names.insert(pos, name.to_string()),
            }
        }
    }

    let mut name_score_sum = 0;
    for (i, name) in names.iter().enumerate() {
        let score = get_name_score(name, (i + 1) as u64);
        name_score_sum += score;
    }

    println!("Total of all name scores in {}: {}", NAME_FILE, name_score_sum);
}
