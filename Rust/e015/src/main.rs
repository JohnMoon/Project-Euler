const GRID_DIM: u128 = 20;

fn factorial(n: u128) -> u128 {
    if n == 1 {
        n
    } else if n == 0 {
        0
    } else {
        n * factorial(n - 1)
    }
}

fn top_half_factorial(start: u128) -> u128 {
    let stop = start / 2;
    let mut n = start;
    let mut ret = 1;
    while n > stop {
        ret *= n;
        n -= 1;
    }
    ret
}

/* Generically, we can find the number of paths by (2n)! / (n!)^2. The lower
 * half of the (2n)! expansion is cancelled by one of the n! expansions, so we
 * only need to calculate the top half of the factorial. That is, we can say
 * binomial_paths(4) = (8 * 7 * 6 * 5) / 4! */
fn binomial_paths(n: u128) -> u128 {
    top_half_factorial(n * 2) / factorial(n)
}

fn main() {
    let paths = binomial_paths(GRID_DIM);
    println!("Number of paths through {}x{} grid: {}",
             GRID_DIM, GRID_DIM, paths);
}
