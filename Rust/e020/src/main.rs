const N: u64 = 100;

fn main() {
    let mut fact_dig = N - 1;
    let mut digit_vec = vec![N];

    while fact_dig >= 1 {
        digit_vec = digit_vec.iter().map(|x| x * fact_dig).collect();
        digit_vec = flatten_carry(&digit_vec);
        fact_dig -= 1;
    }

    let mut sum = 0;
    for dig in digit_vec {
        sum += dig;
    }

    println!("Sum of digits in {}!: {}", N, sum);
}

/* Flattens a vector out into integers with just a one's place, carrying digits
 * when necessary */
fn flatten_carry(nums: &[u64]) -> Vec<u64> {
    let mut ret_vec = Vec::new();
    let mut extras = vec![0];
    let mut need_another_recursion = false;
    for num in nums.iter().rev() {
        let ones_place = *num % 10;
        let extra = (((*num - ones_place) as f64) * 0.1) as u64;

        if extra > 0 {
            need_another_recursion = true;
        }

        extras.insert(0, extra);

        ret_vec.push(ones_place + extras.pop().unwrap());
    }

    let extra = extras.pop().unwrap();
    if extra > 0 {
        ret_vec.push(extra);
    }

    /* Undo the reversal we did in the calculation */
    ret_vec.reverse();

    if need_another_recursion {
        flatten_carry(&ret_vec)
    } else {
        ret_vec
    }
}
