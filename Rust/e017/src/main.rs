const UPPER: u32 = 1000;

fn written_len(n: u32) -> u32 {
    match n {
        0 => 0,
        1 | 2 | 6 | 10 => 3,     // one, two, six, ten
        4 | 5 | 9 => 4,          // four, five, nine
        3 | 7 | 8 => 5,          // three, seven, eight, twelve
        11 | 12 => 6,            // eleven, twelve
        15 | 16 => 7,            // fifteen, sixteen
        13 | 14 | 18 | 19 => 8,  // thirteen, fourteen, eighteen, nineteen
        17 => 9,                 // seventeen
        _ if n >= 20 && n < 30  => 6 + written_len(n % 10), // twenty + extra
        _ if n >= 30 && n < 40  => 6 + written_len(n % 10), // thirty + extra
        _ if n >= 40 && n < 50  => 5 + written_len(n % 10), // forty + extra
        _ if n >= 50 && n < 60  => 5 + written_len(n % 10), // fifty + extra
        _ if n >= 60 && n < 70  => 5 + written_len(n % 10), // sixty + extra
        _ if n >= 70 && n < 80  => 7 + written_len(n % 10), // seventy + extra
        _ if n >= 80 && n < 90  => 6 + written_len(n % 10), // eighty + extra
        _ if n >= 90 && n < 100 => 6 + written_len(n % 10), // ninety + extra
        _ if n == 1000 => 11, // one thousand

        /* Multiple of 100 - hundred + written_len(x) */
        _ if n % 100 == 0 => 7 + written_len((n as f32 * 0.01) as u32),

        /* one hundred _and_ extra */
        _ if n > 100 => written_len(n - (n % 100)) + 3 + written_len(n % 100),

        _ => {
            println!("Unsupported n: {}", n);
            0
        },
    }
}

fn main() {
    let mut sum = 0;

    // four hundred = 11
    assert_eq!(written_len(400), 11);

    // six hundred and one = 16
    assert_eq!(written_len(601), 16);

    // nine hundred and seventy two = 24
    assert_eq!(written_len(972), 24);

    // one thousand = 11
    assert_eq!(written_len(1000), 11);

    for i in 1..=UPPER {
        sum += written_len(i);
    }

    println!("Sum of written lengths: {}", sum);
}
