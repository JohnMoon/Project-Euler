use std::collections::HashMap;

const UPPER_LIMIT: u64 = 28_124;

fn sum_of_factors(n: u64, cache: &mut HashMap<u64, u64>) -> u64 {

    /* If we've already calculated this n, no need to calculate it again */
    if let Some(sum) = cache.get(&n) {
        return *sum
    }

    if n == 1 {
        1
    } else {
        /* Form two vectors that, when concatenated, contain all factors of n.
         * Start with every n being divisible by 1 and itself. */
        let mut left = vec![1];
        let mut right = vec![n];
        let mut i = 2;

        /* Grow vector until left candidate >= first right-side factor */
        while i < *right.first().unwrap() {
            if n % i == 0 {
                left.push(i);
                right.insert(0, (n / i) as u64);
            }

            i += 1;
        }

        /* Append right to left */
        left.append(&mut right);

        /* We don't count N itself in the sum */
        left.pop();

        left.dedup();

        /* Add result to the cache and return it */
        let sum = left.iter().sum();
        cache.insert(n, sum);
        sum
    }
}

fn is_abundant(n: u64, cache: &mut HashMap<u64, u64>) -> bool {
    sum_of_factors(n, cache) > n
}

fn sum_of_two_abundant_nums(n: u64, abundant_nums: &[u64]) -> bool {
    let len = abundant_nums.len();
    let mut i = 0;

    while i < len {
        if n <= abundant_nums[i] {
            return false;
        }

        let diff = n - abundant_nums[i];

        let mut j = i;
        while j < len {
            if abundant_nums[j] == diff {
                return true;
            }
            j += 1;
        }
        i += 1;
    }
    false
}

fn main() {
    /* Cache to store results of factor sum calculations */
    let mut cache = HashMap::new();

    /* Generate list of abundant numbers */
    let mut abundant_nums = Vec::new();
    for n in 1..=UPPER_LIMIT {
        if is_abundant(n, &mut cache) {
            abundant_nums.push(n);
        }
    }

    assert!(sum_of_two_abundant_nums(24, &abundant_nums));

    let mut sum = 0;
    for n in 1..=UPPER_LIMIT {
        if !sum_of_two_abundant_nums(n, &abundant_nums) {
            sum += n;
        }
    }

    println!("Sum of all positive integers which cannot be written as the sum of two abundant numbers: {}",
             sum);
}
