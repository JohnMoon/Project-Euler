fn main() {
    for b in 1..999 {
        for a in 1..999 {
            let c = ((a * a + b * b) as f64).sqrt() as u64;
            if (c * c == (a * a + b * b)) && (a + b + c == 1000) {
                println!("{} * {} * {} == {}", a, b, c, a * b * c);
                return;
            }
        }
    }
}
