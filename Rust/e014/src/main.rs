const START_N_UNDER: u64 = 1_000_000;

fn collatz(start: u64) -> Vec<u64> {
    let mut ret_vec = vec![start];
    loop {
        let n = ret_vec.last().unwrap();
        if n == &1 {
            break;
        }

        if n % 2 == 0 {
            ret_vec.push((n / 2) as u64);
        } else {
            ret_vec.push(3 * n + 1);
        }
    }
    ret_vec
}

fn main() {
    let mut longest_chain = 0;
    let mut longest_chain_start = 1;
    for start in 1..START_N_UNDER {
        let chain = collatz(start);
        if chain.len() > longest_chain {
            longest_chain = chain.len();
            longest_chain_start = start;
        }
    }
    println!("Longest chain ({} terms), starts at {}", longest_chain, longest_chain_start);
}
