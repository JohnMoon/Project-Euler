fn main() {
    let mut sum = 0;
    let mut x = 1;
    let mut y = 1;
    let mut fib = 0;
    while fib < 4_000_000 {
        fib = x + y;
        x = y;
        y = fib;

        if fib % 2 == 0 {
            sum += fib;
        }
    }
    println!("Sum: {}", sum);
}
