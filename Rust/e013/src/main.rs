use std::fs::File;
use std::io::{BufRead, BufReader};

static FILE: &str = "100_50_dig_nums.txt";
const ROW_MAX: usize = 100;
const COL_MAX: usize = 50;

/* Flattens a vector out into integers with just a one's place, carrying digits
 * when necessary */
fn flatten_carry(nums: &[u64]) -> Vec<u64> {
    let mut ret_vec = Vec::new();
    let mut extras = vec![0];
    let mut need_another_recursion = false;
    for num in nums.iter().rev() {
        let ones_place = *num % 10;
        let extra = (((*num - ones_place) as f64) * 0.1) as u64;

        if extra > 0 {
            need_another_recursion = true;
        }

        extras.insert(0, extra);

        ret_vec.push(ones_place + extras.pop().unwrap());
    }

    let extra = extras.pop().unwrap();
    if extra > 0 {
        ret_vec.push(extra);
    }

    /* Undo the reversal we did in the calculation */
    ret_vec.reverse();

    if need_another_recursion {
        flatten_carry(&ret_vec)
    } else {
        ret_vec
    }
}

fn main() {
    let file = File::open(FILE)
        .expect("error opening file");
    let reader = BufReader::new(file);

    let mut grid: Vec<Vec<u64>> = Vec::with_capacity(ROW_MAX);
    for _i in 0..ROW_MAX {
        grid.push(Vec::with_capacity(COL_MAX));
    }

    for (row, line) in reader.lines().enumerate() {
        for num in line.unwrap().chars() {
            grid[row].push(u64::from(num.to_digit(10).unwrap()));
        }
    }

    let mut sums = [0u64; COL_MAX];
    for row in grid {
        for (i, col) in row.iter().enumerate() {
            sums[i] += col;
        }
    }

    let flat_vec = flatten_carry(&sums);

    println!("Sum of numbers in {}:", FILE);
    for (i, n) in flat_vec.iter().enumerate() {
        print!("{}", n);
        if i == 9 {
            print!(" | ");
        }
    }

    println!();
}
