fn main() {
    let mut n = 1;
    loop {
        if evenly_div_by_all(n) {
            break;
        } else {
            n += 1;
        }
    }
    println!("Smallest evenly divisible from 1..20: {}", n);
}

fn evenly_div_by_all(num: u64) -> bool {
    for i in (1..20).rev() {
        if num % i != 0 {
            return false;
        }
    }
    true
}
