public class Euler2 {

	public static void main(String[] args) {
		int sum = 0;
		int x = 1;
		int y = 1;
		int fib = 0;
		while (fib < 4000000) {
			fib = x + y;
			x = y;
			y = fib;
			if ((fib & 1) == 0)
				sum += fib;
		}

		System.out.println("Sum: " + sum);
	}
}
