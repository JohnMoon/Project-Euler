/*
 *
 *   Problem 19
 *
 *      Published on Friday, 14th June 2002, 06:00 pm; Solved by 95462;
 *      Difficulty rating: 5%
 *
 *      You are given the following information, but you may prefer to do some
 *      research for yourself.
 *        * 1 Jan 1900 was a Monday.
 *        * Thirty days has September,
 *          April, June and November.
 *          All the rest have thirty-one,
 *          Saving February alone,
 *          Which has twenty-eight, rain or shine.
 *          And on leap years, twenty-nine.
 *        * A leap year occurs on any year evenly divisible by 4, but not on a
 *          century unless it is divisible by 400.
 *
 *      How many Sundays fell on the first of the month during the twentieth
 *      century (1 Jan 1901 to 31 Dec 2000)?
*/

#include <stdio.h>
#include <stdbool.h>

#define MONTHS 12
#define DAYS 7

struct Date {
	int year; // 1900, 1901...
	int month; // 1 - 12
	int num_days; // 1 - [28 | 29 | 30 | 31]
	int month_day; // 1 ... [28 | 29 | 30 | 31]
	int week_day; // 1 - 7 (Mon - Sun)
};

bool leap_year(int year);
bool equals(struct Date d1, struct Date d2);
struct Date increment_date(struct Date date);

struct Date increment_date(struct Date date)
{
	struct Date new_date;

	/* Find the number of days in this month */
	switch(date.month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			date.num_days = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			date.num_days = 30;
			break;
		case 2:
			if (leap_year(date.year))
				date.num_days = 29;
			else
				date.num_days = 28;
			break;
		default:
			printf("Error - invalid month %d\n", date.month);
	}

	/* Increment the date */
	if (date.month_day + 1 > date.num_days) { // new month
		if (date.month + 1 > MONTHS) { // new year
			new_date.year = date.year + 1;
			new_date.month = 1;
			new_date.month_day = 1;
		}
		else {
			new_date.year = date.year;
			new_date.month = date.month + 1;
			new_date.month_day = 1;
		}
	}
	else {
		new_date.year = date.year;
		new_date.month = date.month;
		new_date.month_day = date.month_day + 1;
	}

	/* Increment the day of the week */
	if (date.week_day + 1 > DAYS)
		new_date.week_day = 1;
	else
		new_date.week_day = date.week_day + 1;

	return new_date;
}

bool leap_year(int year)
{
	return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
}

bool equals(struct Date d1, struct Date d2)
{
	return (d1.month_day == d2.month_day && d1.month == d2.month && d1.year == d2.year);
}

int main()
{
	/* 1 Jan 1901 */
	struct Date start;
	start.month = 1;
	start.year = 1901;
	start.month_day = 1;
	start.week_day = 2;

	/* 31 Dec 2000 */
	struct Date end;
	end.month = 12;
	end.year = 2000;
	end.month_day = 31;
	end.week_day = 7;

	int sum = 0;
	struct Date traversal = start;

	while (!equals(traversal, end)) {
		if (traversal.month_day == 1 && traversal.week_day == 7)
			sum++;
		traversal = increment_date(traversal);
	}

	printf("%d Sundays fell on the first of the month during the twentieth century.\n", sum);
	return 0;
}
