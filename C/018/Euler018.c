/*
 *
 *   Problem 18
 *
 *      Published on Friday, 31st May 2002, 06:00 pm; Solved by 86724;
 *      Difficulty rating: 5%
 *
 *      By starting at the top of the triangle below and moving to adjacent
 *      numbers on the row below, the maximum total from top to bottom is 23.
 *
 *      3
 *      7 4
 *      2 4 6
 *      8 5 9 3
 *
 *      That is, 3 + 7 + 4 + 9 = 23.
 *
 *      Find the maximum total from top to bottom of the triangle below:
 *
 *      75
 *      95 64
 *      17 47 82
 *      18 35 87 10
 *      20 04 82 47 65
 *      19 01 23 75 03 34
 *      88 02 77 73 07 63 67
 *      99 65 04 28 06 16 70 92
 *      41 41 26 56 83 40 80 70 33
 *      41 48 72 33 47 32 37 16 94 29
 *      53 71 44 65 25 43 91 52 97 51 14
 *      70 11 33 28 77 73 17 78 39 68 17 57
 *      91 71 52 38 17 14 91 43 58 50 27 29 48
 *      63 66 04 68 89 53 67 30 73 16 69 87 40 31
 *      04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
 *
 *      NOTE: As there are only 16384 routes, it is possible to solve this
 *      problem by trying every route. However, Problem 67, is the same
 *      challenge with a triangle containing one-hundred rows; it cannot be
 *      solved by brute force, and requires a clever method! ;)
 *
*/

#include <stdio.h>
#define ARR_MAX 15

void find_sum_r(int arr[ARR_MAX][ARR_MAX], int i, int j, int sum);
int largest = 0;

int main()
{
	int arr[ARR_MAX][ARR_MAX] = {
	{75},
	{95, 64},
	{17, 47, 82},
	{18, 35, 87, 10},
	{20, 4,  82, 47, 65},
	{19, 1,  23, 75, 3,  34},
	{88, 2,  77, 73, 7,  63, 67},
	{99, 65, 4,  28, 6,  16, 70, 92},
	{41, 41, 26, 56, 83, 40, 80, 70, 33},
	{41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
	{53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
	{70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
	{91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
	{63, 66, 4,  68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
	{4,  62, 98, 27, 23, 9,  70, 98, 73, 93, 38, 53, 60, 4,  23}};

	find_sum_r(arr, 0, 0, 0);
	printf("Largest sum of all paths: %d\n", largest);

	return 0;
}

/* Recursive function following paths down the triangle */
void find_sum_r(int arr[ARR_MAX][ARR_MAX], int i, int j, int sum)
{
	if (i != ARR_MAX - 1) { // not at bottom
		int new_sum = sum + arr[i][j];
		find_sum_r(arr, i + 1, j, new_sum); // follow path down
		find_sum_r(arr, i + 1, j + 1, new_sum); // follow path right
	}
	else { // at bottom
		sum = sum + arr[i][j]; // adding bottom value
		if (sum > largest)
			largest = sum;
	}
}
