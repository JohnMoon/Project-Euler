/*
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

/* Checks if input is prime */
_Bool is_prime(int x)
{
	if (x < 2)
		return false;
	if (x == 2)
		return true;
	int rt = sqrt(x);
	int i;
	for (i = 3; i < rt; i = i + 2) {
		if (x % i == 0)
			return false;
	}
	return true;
}

/* Finds the largest prime factor of input */
int largest_prime_factor(long test_num)
{
	int largest = 0;
	int rt = sqrt(test_num);
	int i;
	for (i = 3; i < rt; i = i + 2) {
		if (test_num % i == 0 && is_prime(i)) {
			largest = i;
		}
	}
	return largest;
}

int main()
{
	unsigned long test_num = 600851475143;
	int result = largest_prime_factor(test_num);
	printf("Largest prime factor of %lu: %d\n", test_num, result);
	return 0;
}
